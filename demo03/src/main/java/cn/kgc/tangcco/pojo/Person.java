package cn.kgc.tangcco.pojo;

import cn.hutool.core.util.IdcardUtil;
import cn.kgc.tangcco.model.groups.Add;
import cn.kgc.tangcco.model.groups.Update;
import cn.kgc.tangcco.utils.localdate.BaseLocalDateUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.*;
import java.time.LocalDate;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/12 20:41
 */
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public class Person {
    @Null(groups = {Add.class})
    // @NotNull(message = "ID不能为空", groups = {Update.class})
    @NotNull(message = "{person.idNull}", groups = {Update.class})
    private Integer id;

    // @NotBlank(message = "姓名不能为空", groups = {Add.class, Update.class})
    @NotBlank(message = "{person.nicknameNull}", groups = {Add.class, Update.class})
    private String nickname;

    // @NotBlank(message = "手机号不能为空", groups = {Add.class, Update.class})
    // @Pattern(regexp = "^[1][3,4,5,6,7,8,9][0-9]{9}$", message = "请输入正确手机号", groups = {Add.class, Update.class})
    @NotBlank(message = "{person.mobileNull}", groups = {Add.class, Update.class})
    @Pattern(regexp = "^[1][3,4,5,6,7,8,9][0-9]{9}$", message = "{person.mobileRex}", groups = {Add.class, Update.class})
    private String mobile;

    // @NotBlank(message = "邮箱不能为空", groups = {Add.class, Update.class})
    // @Email(message = "请输入正确的邮箱", groups = {Add.class, Update.class})
    @NotBlank(message = "{person.emailNull}", groups = {Add.class, Update.class})
    @Email(message = "{person.emailRex}", groups = {Add.class, Update.class})
    private String email;

    // NotNull(message = "颜值不能为空", groups = {Add.class, Update.class})
    // @Min(value = 1, message = "颜值最小值为{value}", groups = {Add.class, Update.class})
    // @Max(value = 100, message = "颜值最大值为{value}", groups = {Add.class, Update.class})
    // @Range(min = 1, max = 100, message = "颜值越界,颜值范围为1至100之间的整数", groups = {Add.class, Update.class})
    @NotNull(message = "{person.scoreNull}", groups = {Add.class, Update.class})
    @Min(groups = {Add.class, Update.class}, value = 1L)
    @Max(groups = {Add.class, Update.class}, value = 100L)
    private Integer score;

    @NotBlank(message = "{person.idCardNull}", groups = {Add.class, Update.class})
    @Pattern(regexp = "(^\\d{15}$)|(^\\d{18}$)|(^\\d{17}(\\d|X|x)$)", message = "{person.idCardRex}", groups = {Add.class, Update.class})
    private String idCard;

    private Integer age;

    private LocalDate birthday;

    private Integer gender;

    public void setIdCard(String idCard) {
        this.idCard = idCard;
        String pattern = "(^\\d{15}$)|(^\\d{18}$)|(^\\d{17}(\\d|X|x)$)";
        if (idCard.matches(pattern)) {
            this.age = IdcardUtil.getAgeByIdCard(this.idCard);
            this.birthday = BaseLocalDateUtil.parse(IdcardUtil.getBirth(this.idCard), "yyyyMMdd");
            this.gender = IdcardUtil.getGenderByIdCard(this.idCard);
        }

    }

    public Person(@NotBlank(message = "{person.nicknameNull}", groups = {Add.class, Update.class}) String nickname, @NotBlank(message = "{person.mobileNull}", groups = {Add.class, Update.class}) @Pattern(regexp = "^[1][3,4,5,6,7,8,9][0-9]{9}$", message = "{person.mobileRex}", groups = {Add.class, Update.class}) String mobile, @NotBlank(message = "{person.emailNull}", groups = {Add.class, Update.class}) @Email(message = "{person.emailRex}", groups = {Add.class, Update.class}) String email, @NotNull(message = "{person.scoreNull}", groups = {Add.class, Update.class}) @Min(message = "{person.scoreMin}", groups = {Add.class, Update.class}, value = 1L) @Max(message = "{person.scoreNull}", groups = {Add.class, Update.class}, value = 100L) Integer score, @NotBlank(message = "{person.idCardNull", groups = {Add.class, Update.class}) @Pattern(regexp = "(^\\d{15}$)|(^\\d{18}$)|(^\\d{17}(\\d|X|x)$)", message = "{person.idCardRex}", groups = {Add.class, Update.class}) String idCard) {
        this.nickname = nickname;
        this.mobile = mobile;
        this.email = email;
        this.score = score;
        this.idCard = idCard;
    }

    public Person(@Null(groups = {Add.class}) @NotNull(message = "{person.idNull}", groups = {Update.class}) Integer id, @NotBlank(message = "{person.nicknameNull}", groups = {Add.class, Update.class}) String nickname, @NotBlank(message = "{person.mobileNull}", groups = {Add.class, Update.class}) @Pattern(regexp = "^[1][3,4,5,6,7,8,9][0-9]{9}$", message = "{person.mobileRex}", groups = {Add.class, Update.class}) String mobile, @NotBlank(message = "{person.emailNull}", groups = {Add.class, Update.class}) @Email(message = "{person.emailRex}", groups = {Add.class, Update.class}) String email, @NotNull(message = "{person.scoreNull}", groups = {Add.class, Update.class}) @Min(message = "{person.scoreMin}", groups = {Add.class, Update.class}, value = 1L) @Max(message = "{person.scoreNull}", groups = {Add.class, Update.class}, value = 100L) Integer score, @NotBlank(message = "{person.idCardNull", groups = {Add.class, Update.class}) @Pattern(regexp = "(^\\d{15}$)|(^\\d{18}$)|(^\\d{17}(\\d|X|x)$)", message = "{person.idCardRex}", groups = {Add.class, Update.class}) String idCard) {
        this.id = id;
        this.nickname = nickname;
        this.mobile = mobile;
        this.email = email;
        this.score = score;
        this.idCard = idCard;
    }

    public boolean conver() {
        String pattern = "(^\\d{15}$)|(^\\d{18}$)|(^\\d{17}(\\d|X|x)$)";
        if (this.idCard.matches(pattern)) {
            this.age = IdcardUtil.getAgeByIdCard(this.idCard);
            this.birthday = BaseLocalDateUtil.parse(IdcardUtil.getBirth(this.idCard), "yyyyMMdd");
            this.gender = IdcardUtil.getGenderByIdCard(this.idCard);
            return true;
        }
        return false;
    }
}
