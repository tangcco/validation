package cn.kgc.tangcco.controller;

import cn.kgc.tangcco.model.ResponseText;
import cn.kgc.tangcco.model.ResultCode;
import cn.kgc.tangcco.model.groups.Add;
import cn.kgc.tangcco.model.groups.Update;
import cn.kgc.tangcco.pojo.Person;
import cn.kgc.tangcco.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.ListIterator;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/13 17:51
 */
@RestController
@RequestMapping(value = "/person")
public class PersonController {
    @Autowired
    private PersonService personService;

    @PutMapping(value = "/add")
    public ResponseText add(@Validated(value = {Add.class}) Person person, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            List<ObjectError> errors = bindingResult.getAllErrors();
            ListIterator<ObjectError> iterator = errors.listIterator();
            while (iterator.hasNext()) {
                FieldError error = (FieldError) iterator.next();
                String fieldName = error.getField();
                String message = error.getDefaultMessage();
                return new ResponseText(ResultCode.PARAM_ERROR.getCode(), ResultCode.PARAM_ERROR.getMsg(), "属性:" + fieldName + "参数错误 >>> " + message);
            }
        }
        return new ResponseText(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMsg(), person);
    }

    @PutMapping(value = "/update")
    public ResponseText update(@Validated(value = {Update.class}) Person person, BindingResult bindingResult) {
        return personService.update(person, bindingResult);
    }
}
