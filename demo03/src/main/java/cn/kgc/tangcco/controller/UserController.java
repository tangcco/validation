package cn.kgc.tangcco.controller;

import cn.kgc.tangcco.model.ResponseText;
import cn.kgc.tangcco.model.ResultCode;
import cn.kgc.tangcco.model.groups.Add;
import cn.kgc.tangcco.pojo.Person;
import cn.kgc.tangcco.pojo.User;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.ListIterator;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/13 17:51
 */
@RestController
@RequestMapping(value = "/user")
public class UserController {
    @PutMapping(value = "/add")
    public ResponseText add(@Validated User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            List<ObjectError> errors = bindingResult.getAllErrors();
            ListIterator<ObjectError> iterator = errors.listIterator();
            while (iterator.hasNext()) {
                FieldError error = (FieldError) iterator.next();
                String fieldName = error.getField();
                String message = error.getDefaultMessage();
                return new ResponseText(ResultCode.PARAM_ERROR.getCode(), ResultCode.PARAM_ERROR.getMsg(), fieldName + " >>> " + message);
            }
        }
        return new ResponseText(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMsg(), user);
    }
}
