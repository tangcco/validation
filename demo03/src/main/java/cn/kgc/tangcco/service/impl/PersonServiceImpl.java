package cn.kgc.tangcco.service.impl;

import cn.kgc.tangcco.model.ResponseText;
import cn.kgc.tangcco.model.ResultCode;
import cn.kgc.tangcco.model.groups.Add;
import cn.kgc.tangcco.model.groups.Update;
import cn.kgc.tangcco.pojo.Person;
import cn.kgc.tangcco.service.PersonService;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.ListIterator;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/14 7:55
 */
@Service
public class PersonServiceImpl implements PersonService {
    @Override
    public ResponseText update(Person person, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            List<ObjectError> errors = bindingResult.getAllErrors();
            ListIterator<ObjectError> iterator = errors.listIterator();
            while (iterator.hasNext()) {
                FieldError error = (FieldError) iterator.next();
                String fieldName = error.getField();
                String message = error.getDefaultMessage();
                return new ResponseText(ResultCode.PARAM_ERROR.getCode(), ResultCode.PARAM_ERROR.getMsg(), fieldName + " >>> " + message);
            }
        }
        return new ResponseText(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMsg(), person);
    }
}
