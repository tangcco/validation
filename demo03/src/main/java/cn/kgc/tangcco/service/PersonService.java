package cn.kgc.tangcco.service;

import cn.kgc.tangcco.model.ResponseText;
import cn.kgc.tangcco.pojo.Person;
import org.springframework.validation.BindingResult;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/14 7:53
 */
public interface PersonService {
    public ResponseText update(Person person,BindingResult bindingResult);
}
