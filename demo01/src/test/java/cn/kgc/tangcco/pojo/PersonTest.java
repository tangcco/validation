package cn.kgc.tangcco.pojo;

import cn.kgc.tangcco.model.groups.Add;
import cn.kgc.tangcco.model.groups.Update;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import org.hibernate.validator.HibernateValidator;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/12 20:51
 */
public class PersonTest {
    @Test
    public void test01() {
        Person person = new Person();
        person.setMobile("1234567890");
        person.setScore(101);
        person.setEmail("123");
        person.setIdCard("220422");
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Person>> validate = validator.validate(person);
        List<String> list = validate.stream().map(value -> value.getPropertyPath() + "\t" + value.getInvalidValue() + "\t" + value.getMessage()).collect(Collectors.toList());
        list.forEach(msg -> System.out.println(msg));
    }

    @Test
    public void test02() {
        Person person = new Person(null, null, "153", "youxiang", 101, "220422");
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Person>> validate = validator.validate(person);
        List<String> list = validate.stream().map(value -> value.getPropertyPath() + "\t" + value.getInvalidValue() + "\t" + value.getMessage()).collect(Collectors.toList());
        list.forEach(msg -> System.out.println(msg));
    }

    @Test
    public void test03() {
        Person person = new Person();
        person.setMobile("1234567890");
        person.setScore(101);
        person.setEmail("123");
        person.setIdCard("220422");
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Person>> validate = validator.validate(person, Add.class);
        List<String> list = validate.stream().map(value -> value.getPropertyPath() + "\t" + value.getInvalidValue() + "\t" + value.getMessage()).collect(Collectors.toList());
        list.forEach(msg -> System.out.println(msg));
    }

    @Test
    public void test04() {
        Person person = new Person();
        person.setMobile("1234567890");
        person.setScore(101);
        person.setEmail("123");
        person.setIdCard("220422");
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Person>> validate = validator.validate(person, Update.class);
        List<String> list = validate.stream().map(value -> value.getPropertyPath() + "\t" + value.getInvalidValue() + "\t" + value.getMessage()).collect(Collectors.toList());
        list.forEach(msg -> System.out.println(msg));
    }

    @Test
    public void test05() {
        Person person = new Person(null, null, "153", "youxiang", 101, "220422");
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Person>> validate = validator.validate(person, Add.class);
        List<String> list = validate.stream().map(value -> value.getPropertyPath() + "\t" + value.getInvalidValue() + "\t" + value.getMessage()).collect(Collectors.toList());
        list.forEach(msg -> System.out.println(msg));
    }

    @Test
    public void test06() {
        Person person = new Person(null, null, "153", "youxiang", 101, "220422");
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Person>> validate = validator.validate(person, Update.class);
        List<String> list = validate.stream().map(value -> value.getPropertyPath() + "\t" + value.getInvalidValue() + "\t" + value.getMessage()).collect(Collectors.toList());
        list.forEach(msg -> System.out.println(msg));
    }

    @Test
    public void test07() {
        Person person = new Person(null, null, "153", "youxiang", 101, "220422");
        Validator validator = Validation.byProvider(HibernateValidator.class)
                .configure()
                // 快速失败模式
                .failFast(true)
                .buildValidatorFactory()
                .getValidator();

        Set<ConstraintViolation<Person>> validate = validator.validate(person, Update.class);
        List<String> list = validate.stream().map(value -> value.getPropertyPath() + "\t" + value.getInvalidValue() + "\t" + value.getMessage()).collect(Collectors.toList());
        list.forEach(msg -> System.out.println(msg));
    }
}
