package cn.kgc.tangcco.pojo;

import cn.kgc.tangcco.model.ValidationResult;
import cn.kgc.tangcco.model.groups.Add;
import cn.kgc.tangcco.model.groups.Update;
import cn.kgc.tangcco.utils.validation.BaseValidationBeanUtil;
import com.alibaba.fastjson.JSON;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Path;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/13 11:40
 */
public class DeptTest {
    @Test
    public void test01() {
        Dept dept = new Dept();
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Dept>> validate = validator.validate(dept, Add.class);
        List<String> list = validate.stream().map(value -> value.getPropertyPath() + "\t" + value.getInvalidValue() + "\t" + value.getMessage()).collect(Collectors.toList());
        list.forEach(msg -> System.out.println(msg));
    }

    @Test
    public void test02() {
        Dept dept = new Dept();
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Dept>> validate = validator.validate(dept, Update.class);
        List<String> list = validate.stream().map(value -> value.getPropertyPath() + "\t" + value.getInvalidValue() + "\t" + value.getMessage()).collect(Collectors.toList());
        list.forEach(msg -> System.out.println(msg));
    }

    @Test
    public void test03() {
        BaseValidationBeanUtil<Dept> util = new BaseValidationBeanUtil<Dept>();
        List<ValidationResult> list = util.validBean(new Dept(), Add.class);
        list.forEach(result -> System.out.println(JSON.toJSONString(result)));
    }

    @Test
    public void test04() {
        BaseValidationBeanUtil<Dept> util = new BaseValidationBeanUtil<Dept>();
        List<ValidationResult> list = util.validBean(new Dept(), Update.class);
        list.forEach(result -> System.out.println(JSON.toJSONString(result)));
    }
}