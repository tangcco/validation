package cn.kgc.tangcco.pojo;

import cn.kgc.tangcco.model.groups.Update;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import org.hibernate.validator.HibernateValidator;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/13 14:45
 */
public class UserTest {
    @Test
    public void test01() {
        Validator validator = Validation.byProvider(HibernateValidator.class)
                .configure()
                // 快速失败模式
                .failFast(true)
                .buildValidatorFactory()
                .getValidator();

        Set<ConstraintViolation<User>> validate = validator.validate(new User());
        List<String> list = validate.stream().map(value -> value.getPropertyPath() + "\t" + value.getInvalidValue() + "\t" + value.getMessage()).collect(Collectors.toList());
        list.forEach(msg -> System.out.println(msg));
    }

    @Test
    public void test02() {
        Validator validator = Validation.byProvider(HibernateValidator.class)
                .configure()
                // 快速失败模式
                .failFast(true)
                .buildValidatorFactory()
                .getValidator();

        Set<ConstraintViolation<User>> validate = validator.validate(new User(3));
        List<String> list = validate.stream().map(value -> value.getPropertyPath() + "\t" + value.getInvalidValue() + "\t" + value.getMessage()).collect(Collectors.toList());
        list.forEach(msg -> System.out.println(msg));
    }
}
