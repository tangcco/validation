package cn.kgc.tangcco.pojo;
import cn.kgc.tangcco.model.ValidationResult;
import cn.kgc.tangcco.model.groups.Add;
import cn.kgc.tangcco.model.groups.Reference;
import cn.kgc.tangcco.model.groups.Update;
import cn.kgc.tangcco.utils.validation.BaseValidationBeanUtil;
import com.alibaba.fastjson.JSON;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/13 12:19
 */
public class EmpTest {
    @Test
    public void test01() {
        Emp emp = new Emp(null,null,"153","youxiang",101,"220422");
        emp.setDept(new Dept());
        BaseValidationBeanUtil<Emp> util = new BaseValidationBeanUtil<>();
        List<ValidationResult> list = util.validBean(emp, Add.class);
        list.forEach(result -> System.out.println(JSON.toJSONString(result)));
    }
    @Test
    public void test02() {
        Emp emp = new Emp(null,null,"153","youxiang",101,"220422");
        emp.setDept(new Dept());
        BaseValidationBeanUtil<Emp> util = new BaseValidationBeanUtil<>();
        List<ValidationResult> list = util.validBean(emp, Update.class);
        list.forEach(result -> System.out.println(JSON.toJSONString(result)));
    }

    @Test
    public void test03() {
        Emp emp = new Emp(null,null,"153","youxiang",101,"220422",new Dept());
        BaseValidationBeanUtil<Emp> util = new BaseValidationBeanUtil<>();
        List<ValidationResult> list = util.validBean(emp, Reference.class,Add.class);
        list.forEach(result -> System.out.println(JSON.toJSONString(result)));
    }

    @Test
    public void test04(){
        Emp emp = new Emp(null,null,"153","youxiang",101,"220422");
        emp.setDept(new Dept());
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Emp>> validate = validator.validate(emp,Reference.class,Add.class);
        List<String> list = validate.stream().map(value -> value.getPropertyPath() + "\t" + value.getInvalidValue() + "\t" + value.getMessage()).collect(Collectors.toList());
        list.forEach(msg -> System.out.println(msg));
    }
}
