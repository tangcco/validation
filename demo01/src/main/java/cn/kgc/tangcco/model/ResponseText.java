package cn.kgc.tangcco.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author 李昊哲
 * @Description json统一返回格式
 * @date 2020/12/10 下午2:02
 */
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Accessors(chain = true)
public class ResponseText<T> {
    /**
     * 状态码
     */
    private String code;
    /**
     * 状态码含义
     */
    private String msg;
    /**
     * 数据封装
     */
    private T data;
}
