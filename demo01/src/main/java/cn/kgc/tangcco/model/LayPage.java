package cn.kgc.tangcco.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author 李昊哲
 * @Description
 * @create 2020/10/16 14:21
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class LayPage<E> {
    /**
     * 状态码
     */
    private Integer code = 0;
    /**
     * 状态码含义
     */
    private String msg = "";
    /**
     * 总记录数
     */
    private Long count = 0L;
    /**
     * 数据封装
     */
    private List<E> data;

}
