package cn.kgc.tangcco.model.annotation;

import cn.kgc.tangcco.model.Gender;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import jakarta.validation.constraints.NotBlank;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/13 14:01
 */
public class UserGenderValidator implements ConstraintValidator<UserGender, Integer> {

    @Override
    public void initialize(UserGender constraintAnnotation) {

    }

    @Override
    public boolean isValid(Integer integer, ConstraintValidatorContext constraintValidatorContext) {
        if (integer == null) {
            return false;
        }
        Gender[] values = Gender.values();
        for (Gender gender : values) {
            if (gender.getSex().equals(integer)){
                return true;
            }
        }
        return false;
    }
}
