package cn.kgc.tangcco.model.annotation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/13 14:01
 */
@Documented
@Constraint(
        validatedBy = {UserGenderValidator.class}
)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface UserGender {
    String message()

    default "性别的值只能是1或0,1代表男性,0代表女性";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
