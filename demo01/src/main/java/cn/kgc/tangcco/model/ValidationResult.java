package cn.kgc.tangcco.model;

import jakarta.validation.Path;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/13 8:43
 */
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Accessors(chain = true)
public class ValidationResult {
    /**
     * 属性名称
     */
    private String fieldName;
    /**
     * 属性的值
     */
    private Object fieldValue;
    /**
     * 消息提示
     */
    private String message;
}
