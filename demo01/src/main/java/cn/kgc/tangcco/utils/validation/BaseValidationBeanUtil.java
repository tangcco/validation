package cn.kgc.tangcco.utils.validation;

import cn.kgc.tangcco.model.ValidationResult;
import cn.kgc.tangcco.model.groups.Add;
import cn.kgc.tangcco.pojo.Dept;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/13 11:44
 */
public class BaseValidationBeanUtil<T> {
    private static Validator validator;

    static {
        validator = Validation.buildDefaultValidatorFactory().getValidator();

    }


    public List<ValidationResult> validBean(T t) {
        Set<ConstraintViolation<T>> validate = validator.validate(t);
        return validate.stream().map(value -> new ValidationResult(value.getPropertyPath().toString(), value.getInvalidValue(), value.getMessage())).collect(Collectors.toList());
    }

    public List<ValidationResult> validBean(T t, Class<?>... classes) {
        Set<ConstraintViolation<T>> validate = validator.validate(t, classes);
        return validate.stream().map(value -> new ValidationResult(value.getPropertyPath().toString(), value.getInvalidValue(), value.getMessage())).collect(Collectors.toList());
    }


}
