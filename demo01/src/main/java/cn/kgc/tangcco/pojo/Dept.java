package cn.kgc.tangcco.pojo;

import cn.kgc.tangcco.model.groups.Add;
import cn.kgc.tangcco.model.groups.Reference;
import cn.kgc.tangcco.model.groups.Update;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Null;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/13 11:27
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Dept {
    @Null(groups = {Add.class})
    @NotNull(message = "部门ID不能为空", groups = {Reference.class, Update.class})
    private Integer id;

    @Null(groups = {Reference.class})
    @NotBlank(message = "部门名称不能为空", groups = {Add.class, Update.class})
    private String name;

    public List<Emp> emps;

    public Dept(@NotBlank(message = "部门名称不能为空", groups = {Add.class, Update.class}) String name) {
        this.name = name;
    }
}
