package cn.kgc.tangcco.pojo;


import cn.kgc.tangcco.model.annotation.UserGender;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author 李昊哲
 * @Description
 * @create 2021/1/13 13:46
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class User {
    @NotNull(message = "性别不能为空,1代表男性,0代表女性")
    @UserGender
    private Integer gender;
}
